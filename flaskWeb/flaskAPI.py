from flask import Flask, request
from flask_restful import reqparse, abort, Api, Resource
import subprocess
import base64
import cv2
import json
import os
from ocr_google import ocr, getTotalAmountDict

app = Flask(__name__)
api = Api(app)

EncodeReceipt = {}

def testSetEncode():
	imgTestEncodeName = 'iberry1.jpg'
	pathImgTestEncode = '/opt/brand_recog/BBL/ReceiptDetectYolov3/yolo_train_data/'
	with open(str(pathImgTestEncode)+str(imgTestEncodeName), 'rb') as myfile:
		dataBaseGodi = myfile.read()
		EncodeReceipt['encodeImg1'] = (base64.b64encode(dataBaseGodi)).decode('utf-8')

def abort_if_encodeImg_doesnt_exist(encodeImg_id):
    if encodeImg_id not in EncodeReceipt:
        abort(404, message="Todo {} doesn't exist".format(encodeImg_id))

parser = reqparse.RequestParser()
#parser.add_argument('encodeImg')

# Todo
# shows a single encodeImg item and lets you delete a encodeImg item
class Todo(Resource):
    def get(self, encodeImg_id):
        abort_if_encodeImg_doesnt_exist(encodeImg_id)
        return EncodeReceipt[encodeImg_id]

    def delete(self, encodeImg_id):
        abort_if_encodeImg_doesnt_exist(encodeImg_id)
        del EncodeReceipt[encodeImg_id]
        return '', 204

    def put(self, encodeImg_id):
        args = parser.parse_args()
        encodeImg1 = {'encodeImg1': args['encodeImg1']}
        EncodeReceipt[encodeImg_id] = encodeImg1
        return encodeImg1, 200


# TodoList
# shows a list of all encodeImg, and lets you POST to add new receipts
from flask_restful import reqparse
from flask import got_request_exception
class TodoList(Resource):

    def get(self):
        return EncodeReceipt

    def post(self):
        parser.add_argument('encodeImg')
        args = parser.parse_args()
        print('parser')
        #json_data = request.get_json(force=True)
        print('json_data')
        #return json_data['encodeImg'], 200

        if(bool(EncodeReceipt) is False):
            encodeImg_id = 'encodeImg1'
            resultTest_id = 'resultTest1'
        else:
            maxResult = 0
            for key, value in EncodeReceipt.items() :
            	if(key.startswith('encodeImg') and int(key.lstrip('encodeImg'))>maxResult):
                    maxResult =  int(key.lstrip('encodeImg'))
            encodeImg_id = 'encodeImg%i' % maxResult
            resultTest_id =  'resultTest%i' % maxResult
           # encodeImg_id_num = int(max(EncodeReceipt.keys()).lstrip('encodeImg')) + 1
           # encodeImg_id = 'encodeImg%i' % encodeImg_id_num
           # resultTest_id =  'resultTest%i' % encodeImg_id_num
        b64 = args['encodeImg']
        b64_bytes = base64.b64decode(b64)
        with open(os.path.expandvars('$HOME/out.jpg'), 'wb') as f:
            f.write(b64_bytes)

        yoloWeightFileName = 'brand_recog_4300.weights'
        dirDarkNetDefault = '/opt/brand_recog/BBL/ReceiptDetectYolov3/darknet'
        detectorCmd = 'detector'
        modeCmd = 'test'
        dirDarknetDefault = '/opt/brand_recog/BBL/ReceiptDetectYolov3/'
        dirCfgData = str(dirDarknetDefault)+'darknet/cfg/brand.data'
        dirCfgYolo = str(dirDarknetDefault)+'darknet/cfg/brand_recog.cfg'
        dirYoloWeight =  str(dirDarknetDefault)+'darknet/backup2/brand_recog_4300.weights'
        dirFullPicTest =  os.path.expandvars('$HOME/out.jpg')
        # dirFullPicTest = '/opt/brand_recog/BBL/ReceiptDetectYolov3/yolo_train_data/iberry0.jpg'
        r = dn.performDetect(imagePath=dirFullPicTest, thresh= 0.25,
            configPath = dirCfgYolo, weightPath=dirYoloWeight, metaPath=dirCfgData,
            showImage=True, makeImageOnly = False, initOnly=False)
        EncodeReceipt[resultTest_id] = r
        EncodeReceipt[encodeImg_id] = args['encodeImg']

        res = ocr(dirFullPicTest)
        print(res['textAnnotations'][0]['description'])
        x = getTotalAmountDict(res['textAnnotations'], ['total','สุทธิ','รวมทั้งสิ้น','รวมสุทธิ','ยอดสุทธิ','ยอดรวมสุทธิ'])
        print(x)
        r.append(x)
        return r, 200

import darknet as dn
from darknetpy.detector import Detector
class TestPic(Resource):
    #   Show Result Yolov3 From Test Receipt Picture

    def get(self):
        #   Decode
        if(bool(EncodeReceipt) is False):
            return 'None Receipt'
        else:
            encodeImg_id = int(max(EncodeReceipt.keys()).lstrip('encodeImg'))
            encodeImg_id = 'encodeImg%i' % encodeImg_id
            decodeImg = EncodeReceipt[encodeImg_id].encode()
            filename = 'tempPic.jpg'  # I assume you have a way of picking unique filenames
            dirFullPicTest = 'tempPic.jpg'
            b64_bytes = base64.b64decode(decodeImg)
            with open(os.path.expandvars(dirFullPicTest), 'wb') as f:
                f.write(b64_bytes)
#            with open(filename, 'wb') as f:
            	#f.write(decodeImg)
#                f.write(base64.decodebytes(EncodeReceipt[encodeImg_id]))
            print('finish Write Pic')

            #   Test Darknet
            yoloWeightFileName = 'brand_recog_4300.weights'
            dirDarkNetDefault = '/opt/brand_recog/BBL/ReceiptDetectYolov3/darknet'
            detectorCmd = 'detector'
            modeCmd = 'test'
            dirDarknetDefault = '/opt/brand_recog/BBL/ReceiptDetectYolov3/'
            dirCfgData = str(dirDarknetDefault)+'darknet/cfg/brand.data'
            dirCfgYolo = str(dirDarknetDefault)+'darknet/cfg/brand_recog.cfg'
            dirYoloWeight =  str(dirDarknetDefault) + 'darknet/backup2/brand_recog_4300.weights'
#            dirFullPicTest =  '/opt/brand_recog/BBL/ReceiptDetectYolov3/flaskWeb/tempPic.jpg'
            r = dn.performDetect(imagePath=dirFullPicTest, thresh= 0.25,
                configPath = dirCfgYolo, weightPath=dirYoloWeight, metaPath=dirCfgData,
                showImage=True, makeImageOnly = False, initOnly=False)
            return r

class ResultTestPic(Resource):
    #   Show Result Yolov3 From Test Receipt Picture
    def get(self):    #   Decode
        if(bool(EncodeReceipt) is False):
            return 'None Receipt'
        else:
            resultTest_id_num = int(max(EncodeReceipt.keys()).lstrip('resultTest'))
            resultTest_id = 'resultTest%i' % resultTest_id_num
            return EncodeReceipt[resultTest_id]

## Actually setup the Api resource routing here
api.add_resource(TodoList, '/encodeImg')
api.add_resource(TestPic, '/testReceipt')
api.add_resource(Todo, '/encodeImg/<encodeImg_id>')
api.add_resource(ResultTestPic, '/result')

def decodImgDir(encodeImg):
    decodeImg = base64.b64decode(encodeImg)
    return decodeImg

if __name__ == '__main__':
    #testSetEncode()
    app.run(host='0.0.0.0', port='10050',debug=True)
