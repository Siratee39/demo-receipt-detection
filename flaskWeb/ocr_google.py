import io
import os
import sys
import re
import pytesseract
import numpy as np
from Levenshtein import jaro

mode = 0   # 0 = rest, 1 = google.cloud

if mode == 0:
    import requests
    import base64
    import json
    from collections import namedtuple
else:
    from google.cloud import vision
    from google.cloud.vision import types

from PIL import Image

def getIndices(vertices):
    if abs(vertices[3].y - vertices[0].y) > abs(vertices[1].y - vertices[0].y):
        if (vertices[0].x < vertices[1].x):
            #a
            return [0, 1, 2, 3]
        else:
            return [2, 3, 0, 1]
    elif abs(vertices[3].y - vertices[0].y) < abs(vertices[1].y - vertices[0].y):
        if (vertices[0].x > vertices[2].x):
            #b
            return [3, 0, 1, 2]
        else:
            #d
            return [1, 2, 3, 0]
    else:
        assert(False) #not possible?   
    
    return [0, 1, 2, 3]

def getIndicesDict(vertices):
    if abs(vertices[3]['y'] - vertices[0]['y']) > abs(vertices[1]['y'] - vertices[0]['y']):
        if (vertices[0]['x'] < vertices[1]['x']):
            #a
            return [0, 1, 2, 3]
        else:
            return [2, 3, 0, 1]
    elif abs(vertices[3]['y'] - vertices[0]['y']) < abs(vertices[1]['y'] - vertices[0]['y']):
        if (vertices[0]['x'] > vertices[2]['x']):
            #b
            return [3, 0, 1, 2]
        else:
            #d
            return [1, 2, 3, 0]
    else:
        assert(False) #not possible?   
    
    return [0, 1, 2, 3]


def calc_params(ta):
    i = getIndices(ta.bounding_poly.vertices)
    dy1 = ta.bounding_poly.vertices[i[0]].y - ta.bounding_poly.vertices[i[1]].y
    dx1 = ta.bounding_poly.vertices[i[0]].x - ta.bounding_poly.vertices[i[1]].x
    m1 = dy1 / dx1
    c1 = ta.bounding_poly.vertices[i[0]].y - m1 * ta.bounding_poly.vertices[i[0]].x

    dy2 = ta.bounding_poly.vertices[i[3]].y - ta.bounding_poly.vertices[i[2]].y
    dx2 = ta.bounding_poly.vertices[i[3]].x - ta.bounding_poly.vertices[i[2]].x
    m2 = dy2 / dx2
    c2 = ta.bounding_poly.vertices[i[3]].y - m1 * ta.bounding_poly.vertices[i[3]].x
    return (m1, c1, m2, c2)

def calc_paramsDict(ta):
    i = getIndicesDict(ta['boundingPoly']['vertices'])
    dy1 = ta['boundingPoly']['vertices'][i[0]]['y'] - ta['boundingPoly']['vertices'][i[1]]['y']
    dx1 = ta['boundingPoly']['vertices'][i[0]]['x'] - ta['boundingPoly']['vertices'][i[1]]['x']
    m1 = dy1 / dx1
    c1 = ta['boundingPoly']['vertices'][i[0]]['y'] - m1 * ta['boundingPoly']['vertices'][i[0]]['x']

    dy2 = ta['boundingPoly']['vertices'][i[3]]['y'] - ta['boundingPoly']['vertices'][i[2]]['y']
    dx2 = ta['boundingPoly']['vertices'][i[3]]['x'] - ta['boundingPoly']['vertices'][i[2]]['x']
    m2 = dy2 / dx2
    c2 = ta['boundingPoly']['vertices'][i[3]]['y'] - m1 * ta['boundingPoly']['vertices'][i[3]]['x']
    return (m1, c1, m2, c2)


def getTotalAmount(text_annotations, keywords = ["total"]):
    found = False
    for ta in text_annotations:
        t = ta.description.lower() 
        for keyword in keywords:
            score = jaro(keyword, t)
            if (score > 0.85):
                m1, c1, m2, c2 = calc_params(ta)
                found = True
                break
        if found:
            break


    first = True
    for ta in text_annotations:
        if first:
            first = False
            continue
        ctx = (ta.bounding_poly.vertices[0].x + ta.bounding_poly.vertices[2].x) / 2
        cty = (ta.bounding_poly.vertices[0].y + ta.bounding_poly.vertices[2].y) / 2
        py1 = m1 * ctx + c1
        py2 = m2 * ctx + c2
        if (cty >= py1) and (cty <= py2):
            print(ta.description)
            #update param
            if abs((py2 - py1) - (c2 - c1)) < 2:
                m1, c1, m2, c2 = calc_params(ta)

def getTotalAmountDict(text_annotations, keywords = ["total"]):
    found = False
    for ta in text_annotations:
        t = ta['description'].lower() 
        for keyword in keywords:
            score = jaro(keyword, t)
            if (score > 0.85):
                m1, c1, m2, c2 = calc_paramsDict(ta)
                found = True
                break
        if found:
            break

    text = []
    first = True
    for ta in text_annotations:
        if first:
            first = False
            continue
        ctx = (ta['boundingPoly']['vertices'][0]['x'] + ta['boundingPoly']['vertices'][2]['x']) / 2
        cty = (ta['boundingPoly']['vertices'][0]['y'] + ta['boundingPoly']['vertices'][2]['y']) / 2
        py1 = m1 * ctx + c1
        py2 = m2 * ctx + c2
        if (cty >= py1) and (cty <= py2):
            print(ta['description'])
            text.append(ta['description'])
            #update param
            if abs((py2 - py1) - (c2 - c1)) < 2:
                m1, c1, m2, c2 = calc_paramsDict(ta)

    pricetext = ''.join(text)
    return re.search('(\d*,*\d+\.\d{1,2})', pricetext).group(1)

def getLines(response):
    lines = []
    for page in response.full_text_annotation.pages:
        for block in page.blocks:
            #print('\nBlock confidence: {}\n'.format(block.confidence))

            for paragraph in block.paragraphs:
                words = []
                #print('Paragraph confidence: {}'.format(paragraph.confidence))

                for word in paragraph.words:
                    word_text = ''.join([
                        symbol.text for symbol in word.symbols
                    ])
                    #print('Word text: {} (confidence: {})'.format(word_text, word.confidence))
                    words.append(word_text)

                    for symbol in word.symbols:
                        #print('\tSymbol: {} (confidence: {})'.format(symbol.text, symbol.confidence))
                        pass
                lines.append(words)
    return lines

def getLinesDict(response):
    lines = []
    for page in response['fullTextAnnotation']['pages']:
        for block in page['blocks']:
            #print('\nBlock confidence: {}\n'.format(block.confidence))

            for paragraph in block['paragraphs']:
                words = []
                #print('Paragraph confidence: {}'.format(paragraph.confidence))

                for word in paragraph['words']:
                    word_text = ''.join([
                        symbol['text'] for symbol in word['symbols']
                    ])
                    #print('Word text: {} (confidence: {})'.format(word_text, word.confidence))
                    words.append(word_text)

                    for symbol in word['symbols']:
                        #print('\tSymbol: {} (confidence: {})'.format(symbol.text, symbol.confidence))
                        pass
                lines.append(words)
    return lines

def ocr(path, textOnly = False):

    response = None
    content = None

    with io.open(path, 'rb') as image_file:
        content = image_file.read()
    
    if mode == 1:        
        client = vision.ImageAnnotatorClient()
        image = vision.types.Image(content = content)
        response = client.document_text_detection(image = image)
        if textOnly:
            return response.text_annotations[0].description

    elif mode == 0:
        b64b = base64.standard_b64encode(content)
        b64s = b64b.decode("utf-8")
        jsonReq = { "requests": [{ "image": { "content": b64s }, "features": [{ "type": "DOCUMENT_TEXT_DETECTION"}]}]}
        res = requests.post('https://vision.googleapis.com/v1p3beta1/images:annotate?key=AIzaSyDPv-TFgWU4n65HWKUJZKHwfxQOTR9sOeE', json=jsonReq)
        response = res.json()['responses'][0]
        if textOnly:
            return response['textAnnotations'][0]['description']

    return response

def ocr_tess(path, textOnly = False):
    if textOnly:
        text = pytesseract.image_to_string(Image.open(path), lang='eng+tha')
        return text
    #print(text)    
    data = pytesseract.image_to_data(Image.open(path), lang='eng+tha', output_type='dict')
    # process to line, type (text, amount)
    lineIndices = np.array(data['line_num'])
    text = np.array(data['text'])
    numLines = np.max(lineIndices)
    lines = []
    for lineNo in range(numLines):
        linePieces = text[lineNo == lineIndices]
        lineText = ''.join(linePieces).strip()
        #print('|'.join(linePieces).strip())
        if len(lineText) > 0:
            #print(lineText)
            #split number parts from text
            w = re.split(r'(\d+\.\d+|\s+|\d+|[\.,:_])', lineText)
            x = '|'.join(w)
            print(x)
            lines.append(w)
    return lines

if __name__ == '__main__':
    if len(sys.argv) > 1:
        path = sys.argv[1]
    else:
        #path = 'M:\\ML\\data\\receipts\\plaagut\\raw\\8526765687426.jpg'
        #path = 'M:\\ML\\data\\receipts\\iberry\\raw\\4b81f5b841d64354b9c5300340b89917.jpg'
        #path = 'M:\\ML\\data\\receipts\\thai\\20180824_133242.jpg'
        path = 'M:\\ML\\data\\receipts\\starbucks\\starbucks_0029.jpg'

    #print(re.sub("\n|\r", "", ocr_tess(path, True)))
    res = ocr(path)
    lines = getLinesDict(res)
    #print(res.text_annotations[0].description)
    print(res['textAnnotations'][0]['description'])    
    print("--------- Total Extract Below ----------")
    #getTotalAmount(res.text_annotations, ['total', 'สุทธิ', 'รวมทั้งสิ้น', 'รวมสุทธิ', 'ยอดสุทธิ', 'ยอดรวมสุทธิ'])
    x = getTotalAmountDict(res['textAnnotations'], ['total', 'สุทธิ', 'รวมทั้งสิ้น', 'รวมสุทธิ', 'ยอดสุทธิ', 'ยอดรวมสุทธิ'])
    print(x)